# Create your views here.
from django.db.models import Q
from rest_framework import viewsets, generics
from rest_framework.response import Response

from core_app.models import Author, Category, Book
from core_app.serializers import AuthorSerializer, CategorySerializer, BookSerializer, AuthorNameSerializer, \
    BookAuthorSerializer
from core_app.utils import get_most_book_sold_by_author, get_most_book_sold_by_category, get_books_by_author


class AuthorViewSet(viewsets.ModelViewSet):
    """
    ViewSet for Add Author
    """
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer


class CategoryViewSet(viewsets.ModelViewSet):
    """
    ViewSet for Add Category
    """
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class BookViewSet(viewsets.ModelViewSet):
    """
    ViewSet for Add Book
    """
    queryset = Book.objects.all()
    serializer_class = BookSerializer

    def get_queryset(self):
        queryset = Book.objects.all()
        search = self.request.query_params.get('search')
        if search:
            queryset = queryset.filter(Q(title__icontains=search) | Q(
                    author__name__icontains=search))
        return queryset


class GetAuthorNameView(generics.ListAPIView):
    pagination_class = None
    queryset = Author.objects.all()
    serializer_class = AuthorNameSerializer
    action = "list"
    actions_objects = ["view"]

    def list(self, request, *args, **kwargs):
        data = {}
        try:
            queryset = self.filter_queryset(self.get_queryset()).order_by("-id")
            serializer = self.get_serializer(queryset, many=True)
            data = serializer.data

        except Exception as ex:
            print(str(ex))

        return Response(data)


class AuthorMostBooksSoldViewSet(viewsets.ModelViewSet):
    """
    ViewSet for Most Books Sold by Author
    """
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer

    def retrieve(self, request, *args, **kwargs):
        data = {}
        try:
            pk = kwargs['pk']
            most_sold_book = get_most_book_sold_by_author(author_id=pk)
            if most_sold_book:
                serializer = BookSerializer(most_sold_book)
                data = serializer.data

        except Exception as ex:
            print(str(ex))

        return Response(data)


class CategoryMostBooksSoldViewSet(viewsets.ModelViewSet):
    """
    ViewSet for Most Books Sold by Category
    """
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    def retrieve(self, request, *args, **kwargs):
        data = {}
        try:
            pk = kwargs['pk']
            most_sold_book = get_most_book_sold_by_category(category_id=pk)
            if most_sold_book:
                serializer = BookSerializer(most_sold_book)
                data = serializer.data

        except Exception as ex:
            print(str(ex))

        return Response(data)


class BooksByAuthorViewSet(viewsets.ModelViewSet):
    """
    ViewSet for Books By Author
    """
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer

    def retrieve(self, request, *args, **kwargs):
        data = {}
        try:
            pk = kwargs['pk']
            book_qs = get_books_by_author(author_id=pk)
            if book_qs:
                serializer = BookAuthorSerializer(book_qs, many=True)
                data = serializer.data

        except Exception as ex:
            print(str(ex))

        return Response(data)
