from django.contrib import admin

# Register your models here.
from core_app.models import Author, Category, Book


class AuthorAdmin(admin.ModelAdmin):
    fields = ['name', 'phone_number', 'birth_date', 'death_date']
    list_display = ['name', 'phone_number', 'birth_date', 'death_date']
    search_fields = ['name']


class CategoryAdmin(admin.ModelAdmin):
    fields = ['name']
    list_display = ['name']
    search_fields = ['name']


class BookAdmin(admin.ModelAdmin):
    fields = ['book_id', 'title', 'author', 'publisher', 'publish_date', 'category', 'price', 'sold_count']
    list_display = ['book_id', 'title', 'author', 'publisher', 'publish_date', 'category', 'price', 'sold_count']
    search_fields = ['title']


admin.site.register(Author, AuthorAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Book, BookAdmin)
