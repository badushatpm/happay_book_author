from django.db.models import Max

from core_app.models import Book


def get_most_book_sold_by_author(author_id):
    max_sold_count = Book.objects.filter(
        author_id=author_id
    ).aggregate(maxcount=Max('sold_count'))['maxcount']
    book_obj = Book.objects.filter(author_id=author_id, sold_count=max_sold_count).first()
    return book_obj


def get_most_book_sold_by_category(category_id):
    max_sold_count = Book.objects.filter(
        category_id=category_id
    ).aggregate(maxcount=Max('sold_count'))['maxcount']
    book_obj = Book.objects.filter(category_id=category_id, sold_count=max_sold_count).first()
    return book_obj


def get_books_by_author(author_id):
    book_qs = Book.objects.filter(author_id=author_id)
    return book_qs
