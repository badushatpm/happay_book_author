from django.db import models


# Create your models here.
class Author(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    phone_number = models.CharField(max_length=100, blank=True, null=True)
    birth_date = models.CharField(max_length=100, blank=True, null=True)
    death_date = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        if self.name:
            return self.name

    class Meta:
        verbose_name = 'Author'
        verbose_name_plural = 'Authors'
        ordering = ['-id']


class Category(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        if self.name:
            return self.name

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'
        ordering = ['-id']


class Book(models.Model):
    book_id = models.CharField(max_length=100, blank=True, null=True)
    title = models.CharField(max_length=200, blank=True, null=True)
    author = models.ForeignKey(Author, on_delete=models.CASCADE, blank=True, null=True)
    publisher = models.CharField(max_length=100, blank=True, null=True)
    publish_date = models.CharField(max_length=100, blank=True, null=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, blank=True, null=True)
    price = models.CharField(max_length=100, blank=True, null=True)
    sold_count = models.IntegerField(blank=True, null=True)

    def __str__(self):
        if self.title:
            return self.title

    class Meta:
        verbose_name = 'Book'
        verbose_name_plural = 'Book'
        ordering = ['-id']
