from rest_framework import serializers

from core_app.models import Author, Category, Book


class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = (
            'name',
            'phone_number',
            'birth_date',
            'death_date',
        )


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = (
            'name',
        )


class BookSerializer(serializers.ModelSerializer):
    author_name = serializers.SerializerMethodField()

    def get_author_name(self, obj):
        author_name = ""
        author = obj.author
        if author:
            author_name = author.name
        return author_name

    class Meta:
        model = Book
        fields = (
            'book_id',
            'title',
            'author',
            'author_name',
            'publisher',
            'publish_date',
            'category',
            'price',
            'sold_count',
        )


class AuthorNameSerializer(serializers.ModelSerializer):
    author_name = serializers.SerializerMethodField()

    def get_author_name(self, obj):
        return obj.name

    class Meta:
        model = Author
        fields = (
            'id',
            'author_name',
        )


class BookAuthorSerializer(serializers.ModelSerializer):
    author_name = serializers.SerializerMethodField()

    def get_author_name(self, obj):
        author_name = ""
        author = obj.author
        if author:
            author_name = author.name
        return author_name

    class Meta:
        model = Book
        fields = (
            'book_id',
            'title',
            'author_name',
            'publisher',
            'publish_date',
            'price',
            'sold_count',
        )
