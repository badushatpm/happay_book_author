from django.urls import path, include
from rest_framework.routers import DefaultRouter

from core_app.views import AuthorViewSet, CategoryViewSet, BookViewSet, GetAuthorNameView, AuthorMostBooksSoldViewSet, \
    CategoryMostBooksSoldViewSet, BooksByAuthorViewSet

router = DefaultRouter()
router.register(r"author", AuthorViewSet, basename="author")
router.register(r"category", CategoryViewSet, basename="category")
router.register(r"book", BookViewSet, basename="book")
router.register(r"author-most-books-sold", AuthorMostBooksSoldViewSet, basename="author-most-books-sold")
router.register(r"category-most-books-sold", CategoryMostBooksSoldViewSet, basename="category-most-books-sold")
router.register(r"author-books", BooksByAuthorViewSet, basename="author-books")

urlpatterns = [
    path("", include(router.urls)),
    path("author-name-list/", GetAuthorNameView.as_view(), name="author-name-list"),
]
