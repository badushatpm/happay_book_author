# README #

This README would normally document whatever steps are necessary to get your application up and running.
This is the Project to provide APIs for Happay_Book_Author application

### Steps to run the project ###

* Clone the project repository from bitbucket
* git clone https://badushatpm@bitbucket.org/badushatpm/happay_book_author.git
* Create a virtual environment for python
* Run command: pip install -r requirements.txt
* Run migrations script: python manage.py migrate

### APIs added in the project ###

* Add an Author - base_url/author/ - method POST
* Add Book to Catalog - base_url/book/ - method POST
* Add Category - base_url/category/ - method POST
* Get List of Categories - base_url/category/ - method GET
* Get All Author Name - base_url/author-name-list/ - method GET
* Get Most Books Sold By Author - base_url/author-most-books-sold/<author ID>/ - method GET
* Get Most Books Sold By Category - base_url/category-most-books-sold/<category ID>/ - method GET
* Search Book - base_url/book/?search=(<by_partial_title/by_partial_author_name> or both) - method GET
* Get Books By Author - base_url/author-books/<author ID>/ - method GET
